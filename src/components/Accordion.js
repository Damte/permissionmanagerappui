import React, { useState, useRef } from "react";
import Chevron from "../components/Chevron";
import ContentTable from "./ContentTable";
import SelectComponent from "./SelectComponent";
// import SelectComponent from "./ReactSelectAsync";
import "./Accordion.css";

function Accordion(props) {

  const [setActive, setActiveState] = useState("");
  const [setHeight, setHeightState] = useState("0px");
  const [setRotate, setRotateState] = useState("accordion__icon");

  const content = useRef(null);
  const items = { content };

  function toggleAccordion() {
    setActiveState(setActive === "" ? "active" : "");
    console.log(content.current.scrollHeight);
    setHeightState(
      setActive === "active" ? "0px" : `${content.current.scrollHeight}px`
    );
    setRotateState(
      setActive === "active" ? "accordion__icon" : "accordion__icon rotate"
    );
  }
  return (
    <div className="accordion__section">
      <button className= {`accordion ${setActive}`} onClick={toggleAccordion} >
        <div className="container border" >
          <div className="row ml-3">
            <p className="accordion__title">
              <span className="h5">
                Main permission {props.entityId}: {""}
                <span className="h4 ml-2 text-capitalize font-weight-bold">{props.title}</span>
              </span>
            </p>
            <Chevron className={`${setRotate}`} width={10} fill={"#777"} />
          </div>
          <div className="row">
            <SelectComponent
              primaryPermissionId={props.entityId}
              secondaryPermissions={props.content}
            />
          </div>
        </div>
      </button>
      <div
        ref={content}
        style={{ maxHeight: `${setHeight}` }}
        className="accordion__content"
      >
        <ContentTable
          roleId={props.roleId}
          primaryPermissionId={props.entityId}
          secondaryPermissions={props.content}
        />
        <div className="accordion__text" />
      </div>
    </div>
  );
}

export default Accordion;
