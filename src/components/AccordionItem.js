import React from 'react';
import Button from 'react-bootstrap/Button'


function AccordionItem({thingie}) {
    console.log("inside accordion item")
    console.log({thingie})
    return (
        <div>
            <ul>
                {thingie.map((secondaryItem, id) => (
                    <li key={id}>{secondaryItem.name} <span> <Button className="btn-sm btn-danger float-left">-</Button></span>
                        <ul>
                            {
                                  (secondaryItem!==undefined && secondaryItem.tertiaryPermissions!==undefined) ? (
                                    secondaryItem.tertiaryPermissions.map((item, id) => <li key={id}>{item.name} <span><Button className="btn-sm btn-warning">-</Button></span></li>)
                                  )
                                    : <li> No Message Found </li>
                            }
                           
                        </ul>
                    </li>
                ))}
            </ul>
            <h1>Item</h1>
        </div>
    )
}

export default AccordionItem;