import React from "react";
import Button from "react-bootstrap/Button";

function MinusButtonTertiary({ permissionId, parentPermissionId }) {
  // update permission in DB
  const updatePermission = () => {
    const myPost = {
      active: false,
      parentPermissionId: parentPermissionId,
    };

    const options = {
      method: "PATCH",
      body: JSON.stringify(myPost),
      headers: {
        "Content-Type": "application/json",
        mode: "no-cors",
      },
    };

    fetch("http://localhost:8091/tertiary-permission/" + permissionId, options)
      .then((res) => res.json())
      .then((data) => {
        alert("well done, the permission with id " + permissionId + " has been removed from the list");
      });
  };

  return (
    <>
      <Button
        className="btn-sm btn-dark float-right"
        onClick={updatePermission}
      >
        -
      </Button>
    </>
  );
}

export default MinusButtonTertiary;
