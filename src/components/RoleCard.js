import React, { useState, useEffect } from "react";
import Accordion from "./Accordion";
import Button from "react-bootstrap/Button";
import AsyncSelect from "react-select/async";
import "./RoleCard.css";

function RoleCard(props) {
  const [permissions, setPermissions] = useState([]);
  const [selectedValue, setSelectedValue] = useState(null);
  const [accordionComponent, setAccordionComponent] = useState();

  useEffect(() => {
    setAccordionComponent(
      <div>
        {permissions.map((permission) => (
          <Accordion
            key={permission.entityId}
            title={permission.name}
            entityId={permission.entityId}
            roleId={props.roleId}
            content={permission.secondaryPermissions.map(
              (secondaryPermission) => {
                return secondaryPermission;
              }
            )}
          />
        ))}
      </div>
    );
  }, [permissions]);

  const fetchData = () => {
    fetch("http://localhost:8091/role/" + props.roleId + "/primary-permissions")
      .then((response) => response.json())
      .then((data) => setPermissions(data));
  };

  useEffect(() => {
    fetchData();
  }, [permissions]);

  const loadOptions = () => {
    return fetch(`http://localhost:8091/primary-permissions`).then((res) =>
      res.json()
    );
  };

  // handle selection
  const handleChange = (value) => {
    setSelectedValue(value);
  };

  const addPermission = () => {
    const permissionToUpdate = {
      name: selectedValue.name,
      primaryPermissionId: selectedValue.entityId,
    };

    const options = {
      method: "PATCH",
      body: JSON.stringify(permissionToUpdate),
      headers: {
        "Content-Type": "application/json",
        mode: "no-cors",
      },
    };

    fetch("http://localhost:8091/role/"+props.roleId+"/primary-permission", options)
      .then((res) => {
        if (res.status === 202) {
          alert(
            selectedValue.name + " has been added to the list of permissions"
          )
        } 
        return  res.json();
      })
      .then((data) => {
        if (data.status != 202) {
          alert(data.message)
        }
      });
  };

  const removePermission = () => {
    const permissionToUpdate = {
      "name": selectedValue.name,
      "primaryPermissionId": selectedValue.entityId
    };

    const options = {
      method: "DELETE",
      body: JSON.stringify(permissionToUpdate),
      headers: {
        "Content-Type": "application/json",
        mode: "no-cors",
      },
    };

    fetch("http://localhost:8091/role/"+props.roleId+"/primary-permission", options)
      .then((res) => {
        if (res.status === 202) {
          alert(
            selectedValue.name + " has been removed from the list of permissions"
          );
        }
        console.log(res);
        return res.json();
      })
      .then((data) => {
        // console.log(data);
      });

  };

  return (
    <div className="container col-md-10 offset-1">
      <div className="card card-body">
        <p className="display-4">{props.name} </p>
        <p className="card-text">
          The following main permissions have been currently granted to this
          role:
          <ul className="mt-2">
            {permissions.map((permission) => (
              <li className="h5 text-capitalize">{permission.name}</li>
            ))}
          </ul>
        </p>
        <div className="p-2 offset-3 col-md-6">
          <AsyncSelect
            placeholder="Enter space and select from the list"
            cacheOptions
            defaultOptions
            getOptionLabel={(e) => e.name}
            getOptionValue={(e) => e.entityId}
            loadOptions={loadOptions}
            onChange={handleChange}
          />

        </div>
        <div class="btn-group offset-1 col-md-10">
            <Button className="m-1 btn-info" onClick={addPermission}>
              Add Permission
            </Button>
            <Button className="m-1 btn-info" onClick={removePermission}>
              Remove Permission
            </Button>
          </div>
        <p className="card-text">
          In the following carousel you can find and edit information about the
          secondary and tertiary permissions currently adjudicated to{" "}
          {props.name}.
        </p>
        {accordionComponent}
      </div>
    </div>
  );
}

export default RoleCard;
