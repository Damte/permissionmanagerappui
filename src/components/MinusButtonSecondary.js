import React from "react";
import Button from "react-bootstrap/Button";

function MinusButtonSecondary({ permissionId, primaryPermissionId }) {
  // update permission in DB
  const updatePermission = () => {
    const myPost = {
      active: false,
      primaryPermissionId: primaryPermissionId,
    };

    const options = {
      method: "PATCH",
      body: JSON.stringify(myPost),
      headers: {
        "Content-Type": "application/json",
        mode: "no-cors",
      },
    };

    fetch("http://localhost:8091/secondary-permission/" + permissionId, options)
      .then((res) => res.json())
      .then((data) => {
        alert("well done, the permission with id " + permissionId + " has been removed from the list");
      });
  };

  return (
    <>
      <Button
        className="btn-sm btn-dark float-right"
        onClick={updatePermission}
      >
        -
      </Button>
    </>
  );
}

export default MinusButtonSecondary;
