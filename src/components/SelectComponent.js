import React, { useState} from "react";
import AsyncSelect from "react-select/async";
import Button from "react-bootstrap/Button";



function SelectComponent(props) {
  const [selectedValue, setSelectedValue] = useState(null);
  const [radio, setRadio] = useState("");

  const getParentId = (selectedItem) => {
    var secondaryPermissionId
    ;
    props.secondaryPermissions.forEach((element) => {
      element.tertiaryPermissions.forEach((innerElement) => {
        if (innerElement.name === selectedItem.name) {
          secondaryPermissionId = element.id;
        };
      });
    });
    return secondaryPermissionId
  };
  // handle selection
  const handleChange = (value) => {
    setSelectedValue(value);
  };

  // load options using API call
  const loadOptions = () => {
    return fetch(`http://localhost:8091/${radio}`).then((res) => res.json());
  };

    // update API request to the radio buttom
  const getUrl = (radio, id) => {
      var str = radio.substring(0, radio.length - 1);
      return 'http://localhost:8091/'+str+'/' + id;
  };
  
  const getPermissionBody = (radio, parentId) => {
    if (radio.includes("secondary")) {
      return {
        "active": !selectedValue.active,
        "primaryPermissionId": props.primaryPermissionId
      }
    } else {
      return {
        "active": !selectedValue.active,
        "parentPermissionId": parentId,

      }
    }
  }


    // update permission in DB
  const updatePermission = () => {
    const parentId = getParentId(selectedValue)

    const updatePermission = getPermissionBody(radio, parentId);

    
    const options = {
      method: 'PATCH',
      body: JSON.stringify(updatePermission),
      headers: {
        'Content-Type': 'application/json',
        'mode': 'no-cors',
      }
    };

    let url = getUrl(radio, selectedValue.id);

    
    fetch(url, options)
      .then(res => {
        if (res.status === 202) {
          alert(selectedValue.name + " has been added to the list of permissions")
        } 
        console.log(res); 
        return res.json()
      })
      .then(data => {
        if (data.status !== 202) {
          alert(data.message)
        }
      });
  };
  

  return (
    <>
      <div className="container">
        <div className="col-md-12">
          <AsyncSelect
            placeholder="Please select the permission type, then enter any key and select from the list"
            cacheOptions
            defaultOptions
            getOptionLabel={(e) => e.name}
            getOptionValue={(e) => e.entityId}
            loadOptions={loadOptions}
            onChange={handleChange}
          />
          <Button className="mt-3 btn-info" onClick={updatePermission}>Add Permission</Button>
          <div className="row mt-3">
            <label class="radio-inline col-md-4 offset-md-2">
              <input
                type="radio"
                name="optradio"
                checked={radio === "secondary-permissions"}
                value="secondary-permissions"
                onChange={(e) => {
                  setRadio(e.target.value);
                }}
              />
              Secondary Permission
            </label>
            <label class="radio-inline col-md-4">
              <input
                type="radio"
                name="optradio"
                checked={radio === "tertiary-permissions"}
                value="tertiary-permissions"
                onChange={(e) => {
                  setRadio(e.target.value);
                }}
              />
              Tertiary Permission
            </label>
          </div>
        </div>
      </div>
    </>
  );
}

export default SelectComponent;
