import React, {useState, useEffect} from "react";
import MinusButtonSecondary from "./MinusButtonSecondary";
import MinusButtonTertiary from "./MinusButtonTertiary";
import "./ContentTable.css";
import Table from "react-bootstrap/Table";

function ContentTable(props) {
  const [permissions, setPermissions] = useState([]);
  const [filteredPrimaryPermissions, setFilteredPrimaryPermissions] = useState([]);
  const [filteredSecondaryPermissions, setFilteredSecondaryPermissions] = useState(props.secondaryPermissions.filter(secondaryPermission => secondaryPermission.active === true));
  const [tableBody, setTableBody] = useState(
    <tbody>
      {props.secondaryPermissions !== undefined ? (
        
        props.secondaryPermissions.map(secondaryPermission =>
       
            (<tr key={secondaryPermission.id}>
          <td>{secondaryPermission.id}</td>
          <td>{secondaryPermission.name} <span> <MinusButtonSecondary primaryPermissionId={ props.primaryPermissionId} permissionId = {secondaryPermission.id}/></span></td>
          <td>
            {secondaryPermission.tertiaryPermissions !== undefined ? (
              secondaryPermission.tertiaryPermissions.filter(tertiaryPermission => (tertiaryPermission.active === true)).map((item, id) => (
                <li className="p-1 ml-5 text-left"key={id}>{item.name} <span><MinusButtonTertiary parentPermissionId={ secondaryPermission.id} permissionId = {item.id}/></span></li>
              ))
            ) : (
                <li> Nothing here mate </li>
              )}
          </td>
        </tr>)
        )
      ) :
        (
          <li> What what? </li>
        )
      }
    </tbody>
  );


  
  useEffect(() => {
    fetch("http://localhost:8091/role/" + props.roleId + "/primary-permissions")
      .then((response) => response.json())
      .then((data) => setPermissions(data));
  }, [permissions]);

  useEffect(() => {
    setFilteredPrimaryPermissions(permissions.filter(primaryPermission => primaryPermission.entityId === props.primaryPermissionId));
  }, [permissions]);

  useEffect(() => {
    filteredPrimaryPermissions.forEach((primaryPermission) => {
      let secondaryPermissions = primaryPermission.secondaryPermissions;
      setFilteredSecondaryPermissions(secondaryPermissions.filter(secondaryPermission => secondaryPermission.active === true))
    })
    setTableBody(
      <tbody>
      {filteredSecondaryPermissions !== undefined ? (
        
        filteredSecondaryPermissions.map(secondaryPermission =>
       
            (<tr key={secondaryPermission.id}>
          <td>{secondaryPermission.id}</td>
          <td>{secondaryPermission.name} <span> <MinusButtonSecondary primaryPermissionId={ props.primaryPermissionId} permissionId = {secondaryPermission.id}/></span></td>
          <td>
            {secondaryPermission.tertiaryPermissions !== undefined ? (
              secondaryPermission.tertiaryPermissions.filter(tertiaryPermission => (tertiaryPermission.active === true)).map((item, id) => (
                <li className="p-1 ml-5 text-left"key={id}>{item.name} <span><MinusButtonTertiary parentPermissionId={ secondaryPermission.id} permissionId = {item.id}/></span></li>
              ))
            ) : (
                <li> Nothing here mate </li>
              )}
          </td>
        </tr>)
        )
      ) :
        (
          <li> What what? </li>
        )
      }
    </tbody>
    )
  }, [filteredPrimaryPermissions]);

  return (
    <Table class="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Secondary Permission</th>
          <th scope="col">Tertiary Permission</th>
        </tr>
      </thead>
      {tableBody}
    </Table>
  );
}

export default ContentTable;
