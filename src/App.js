import './App.css';
import React, { useState, useEffect } from 'react';
import RoleCard from './components/RoleCard';

function App() {

    const [roles, setRoles] = useState([])

  useEffect(() => {
    fetch('http://localhost:8091/roles')
  .then((response) => response.json())
      .then((data) => setRoles(data));

  }, [])
    console.log("roles")
    console.log(roles)
  return (
    <div className="App">
      <div>
        {roles.map(role => (
          <RoleCard key={role.roleId} {...role}/>
        ))}
   </div>
    </div>
  );
}

export default App;
